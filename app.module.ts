import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponentComponent } from './dashboard-component/dashboard-component.component';
import { JoinComponenetComponent } from './join-componenet/join-componenet.component';
import { AssignmentsComponenetComponent } from './assignments-componenet/assignments-componenet.component';
import { CourcesCompoenentComponent } from './cources-compoenent/cources-compoenent.component';
import { RouterModule } from '@angular/router';
import { HomeComponentComponent } from './home-component/home-component.component';
import { LoginComponenetComponent } from './login-componenet/login-componenet.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponentComponent,
    JoinComponenetComponent,
    AssignmentsComponenetComponent,
    CourcesCompoenentComponent,
    HomeComponentComponent,
    LoginComponenetComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule.forRoot([

      {
        path: 'home', 
      component: HomeComponentComponent
    },
      {
        path: 'dashboard', 
      component: DashboardComponentComponent
      },
      
      {
        path: 'assignments', 
      component: AssignmentsComponenetComponent 
      },

      {
        path: 'cources', 
      component: CourcesCompoenentComponent
      },

      {
        path: 'join', 
      component: JoinComponenetComponent
      },
      
      {
        path: 'login', 
      component: LoginComponenetComponent
      },

    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
