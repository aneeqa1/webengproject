import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignmentsComponenetComponent } from './assignments-componenet.component';

describe('AssignmentsComponenetComponent', () => {
  let component: AssignmentsComponenetComponent;
  let fixture: ComponentFixture<AssignmentsComponenetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignmentsComponenetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmentsComponenetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
