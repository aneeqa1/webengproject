import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourcesCompoenentComponent } from './cources-compoenent.component';

describe('CourcesCompoenentComponent', () => {
  let component: CourcesCompoenentComponent;
  let fixture: ComponentFixture<CourcesCompoenentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourcesCompoenentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourcesCompoenentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
