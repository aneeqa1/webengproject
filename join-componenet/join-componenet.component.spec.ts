import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JoinComponenetComponent } from './join-componenet.component';

describe('JoinComponenetComponent', () => {
  let component: JoinComponenetComponent;
  let fixture: ComponentFixture<JoinComponenetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JoinComponenetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JoinComponenetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
